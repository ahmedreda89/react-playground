import styled from 'styled-components'

export const HomeCard = styled.div`
    position: relative;
    margin: 20px;
    height: 200px;
    display: inline-block;
    img{
        max-height:100%;
    }
`

export const HomeWrapper = styled.div`
    text-align: center;
`

export const HomeCardTitle = styled.h3`
    position: absolute;
    bottom: -40px;
    color: #000;
`
