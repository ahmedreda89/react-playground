import React from 'react'
import { Link } from "react-router-dom"

import { HomeWrapper, HomeCard, HomeCardTitle } from './styled'
import charactersPoster from '../common/assets/stars3.jpg'
import planetsPoster from '../common/assets/stars2.jpeg'

export default function Home (props) {
    return (
        <HomeWrapper>
           <HomeCard>
               <Link to="/planets">
                   <img alt="Planets" src={planetsPoster} />
                   <HomeCardTitle>Planets</HomeCardTitle>
               </Link>
           </HomeCard>

           <HomeCard>
               <Link to="/characters">
                   <img alt="Characters" src={charactersPoster} />
                   <HomeCardTitle>Characters</HomeCardTitle>
               </Link>
           </HomeCard>
        </HomeWrapper>
    )
}
