import { inCase } from '../common/utils'
import { actionTypes } from './planetsActions'

const defaultState = {
	list: {
		results: [],
		meta: {},
	},
	meta: {},
}

export default inCase.curry('planetsReducer')
	.condition((_, action) => action.type)
	.otherwise((state) => state || defaultState)
	.cases({
		[actionTypes.FETCH_ALL_PLANETS]: fetchAllPlanets,
		[actionTypes.FETCH_ALL_PLANETS_FAILED]: fetchAllPlanetsFailed,
		[actionTypes.FETCH_ALL_PLANETS_COMPLETED]: fetchAllPlanetsCompleted,
	})

//
function fetchAllPlanets(store, action) {
	console.log('[planetsReducer] fetchAllPlanets::action', action)

	return {
		...store,
		list: {
			...store.list,
			meta: {
				...store.list?.meta,
				isLoading: true,
				isRequested: true,

				isFailed: null,
				isCompleted: null,
			},
		},
	}
}

function fetchAllPlanetsCompleted(store, action) {
	console.log('[charactersReducer] fetchAllCharactersCompleted::action', action)

	return {
		...store,
		list: {
			...store.list,
			results: action?.payload,
			meta: {
				...store.list?.meta,
				isLoading: false,
				isRequested: null,

				isFailed: false,
				isCompleted: true,
			},
		},
	}
}

function fetchAllPlanetsFailed(store, action) {
	console.log('[developersReducer] fetchAllServicesFailed::action', action)

	return {
		...store,
		list: {
			...store.list,
			meta: {
				...store.list?.meta,
				isLoading: false,
				isRequested: null,

				isFailed: true,
				isCompleted: false,

				error: action.payload,
			},
		},
	}
}
