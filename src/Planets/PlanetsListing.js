import React from 'react'
import {connect} from 'react-redux'

import Listing from "../common/Listing"
import {dispatch} from '../common/store'
import { PageTitle } from '../common/styled'
import {actions} from '../Planets/planetsActions'



function PlanetsListing (props) {
    React.useEffect(() => {
        props.fetchAllPlanets()
    }, [props.fetchAllPlanets])

    const columns = [
        {title: 'Name', value: 'name'},
        {title: 'Rotation Period', value: 'rotation_period'},
        {title: 'Diameter', value: 'diameter'},
        {title: 'Gravity', value: 'gravity'},
        {title: 'Surface Water', value: 'surface_water'},
        {title: 'Population', value: 'population'},
    ]

    return (
      <>
          <PageTitle>Planets</PageTitle>

          <Listing
            columns={columns}
            data={props.data}
            error={props.error}
            failed={props.isFailed}
            loading={props.isLoading}
          />
      </>
    )
}


function mapStateToProps({ Planets }) {
    return {
        data: Planets?.list?.results,
        error: Planets?.list?.meta?.error,
        isFailed: Planets?.list?.meta?.isFailed,
        isLoading: Planets?.list?.meta?.isLoading,
        isCompleted: Planets?.list?.meta?.isCompleted,
    }
}

function mapDispatchToProps(ownProps) {
    return {
        fetchAllPlanets: (...args) => {
            console.log('[PlanetsListing]fetchAllPlanets', args)
            dispatch(actions.fetchAllPlanets(...args))

            // eslint-disable-next-line no-unused-expressions
            ownProps?.fetchAllPlanets?.(...args)
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlanetsListing)
