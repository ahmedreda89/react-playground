import { camelCase as _camelCase, get as _get } from 'lodash'

import { renameFunc } from '../common/utils/utils'

export const actionTypes = {
    FETCH_ALL_PLANETS: 'planets/listing/fetchAll 🚀 💬',
    FETCH_ALL_PLANETS_COMPLETED: 'planets/listing/fetchAll 🚀 ✅',
    FETCH_ALL_PLANETS_FAILED: 'planets/listing/fetchAll  🚀 ❌',
    FLUSH_PLANETS_DATA: 'planets/all [flush] 🚽',
}

export const actions = Object.keys(actionTypes)
    .reduce((accum, id) => {
        const creatorName = _camelCase(id)

        const creatorFunction = function _(payload, meta) {
            return {
                type: _get(actionTypes, id),
                payload,
                meta,
            }
        }

        /* eslint-disable no-param-reassign */
        accum[creatorName] = renameFunc(creatorFunction, creatorName)
        /* eslint-enable no-param-reassign */

        return accum
    }, {})