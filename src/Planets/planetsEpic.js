import * as Rx from 'rxjs'
import { ofType } from 'redux-observable'
import { switchMap, map, catchError } from 'rxjs/operators'

import { observableFromPromiseProperties } from '../common/utils/utils'

import _planetsAPIs from './api'
import { actionTypes, actions } from './planetsActions'

const planetsAPIs = observableFromPromiseProperties(_planetsAPIs, Rx)
const xx = planetsAPIs.fetchAllPlanets
export const planetsListing = action$ => action$.pipe(
	ofType(actionTypes.FETCH_ALL_PLANETS),
	// delay(1000), // Asynchronously wait 1000ms then continue
	switchMap(action =>
		planetsAPIs.fetchAllPlanets()
			.pipe(
				map((response) => {
					return actions.fetchAllPlanetsCompleted(response?.body?.results)
				}),
				catchError( (error) => {
					return Rx.of(actions.fetchAllPlanetsFailed(error.message))
				})
			)
	)
)

export default [
	planetsListing,
]