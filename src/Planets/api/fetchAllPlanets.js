import request from 'superagent'

export default function fetchAllPlanets(queries) {
  return new Promise((resolve, reject) => {
    request
      .get('http://swapi.py4e.com/api/planets')
      .then((resp) => {
        resolve(resp)
      })
      .catch((error) => {
        reject(error)
      })
  })
}