import React from 'react'
import { Provider } from 'react-redux'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom"

import Home from './Home'
import Planets from './Planets'
import store from './common/store'
import Header from './common/Header'
import Characters from './Characters'
import { PageWrapper } from './common/styled'

function App() {
  const renderRoutes = () => {
    return (
        <Switch>
          <Redirect exact path="/" to="/home" />

          <Route path="/home" component={Home}/>
          <Route exact path="/planets" component={Planets}/>
          <Route exact path="/characters" component={Characters}/>
        </Switch>
    )
  }

  return (
      <Provider store={store}>
          <div className="App" style={{ float: 'left', width: '100%' }}>
            <Router>
                <Header />
                <PageWrapper>
                    {renderRoutes()}
                </PageWrapper>
            </Router>
          </div>
      </Provider>
  )
}

export default App
