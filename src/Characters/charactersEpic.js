import * as Rx from 'rxjs'
import { ofType } from 'redux-observable'
import { switchMap, map, catchError } from 'rxjs/operators'

import { observableFromPromiseProperties } from '../common/utils/utils'

import _charactersAPIs from './api'
import { actionTypes, actions } from './charactersActions'

const charactersAPIs = observableFromPromiseProperties(_charactersAPIs, Rx)

export const charactersListing = action$ => action$.pipe(
  ofType(actionTypes.FETCH_ALL_CHARACTERS),
  // delay(1000), // Asynchronously wait 1000ms then continue
  switchMap(action =>
    charactersAPIs.fetchAllCharacters()
      .pipe(
        map((response) => {
          return actions.fetchAllCharactersCompleted(response?.body?.results)
        }),
        catchError( (error) => {
          return Rx.of(actions.fetchAllCharactersFailed(error.message))
        })
      )
  )
)

export default [
  charactersListing,
]