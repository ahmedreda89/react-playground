import { camelCase as _camelCase, get as _get } from 'lodash'

import { renameFunc } from '../common/utils/utils'

export const actionTypes = {
    FETCH_ALL_CHARACTERS: 'characters/listing/fetchAll 🚀 💬',
    FETCH_ALL_CHARACTERS_COMPLETED: 'characters/listing/fetchAll 🚀 ✅',
    FETCH_ALL_CHARACTERS_FAILED: 'characters/listing/fetchAll 🚀 ❌',
    FLUSH_CHARACTERS_DATA: 'characters/all [flush] 🚽',
}

export const actions = Object.keys(actionTypes)
    .reduce((accum, id) => {
        const creatorName = _camelCase(id)

        const creatorFunction = function _(payload, meta) {
            return {
                type: _get(actionTypes, id),
                payload,
                meta,
            }
        }

        /* eslint-disable no-param-reassign */
        accum[creatorName] = renameFunc(creatorFunction, creatorName)
        /* eslint-enable no-param-reassign */

        return accum
    }, {})