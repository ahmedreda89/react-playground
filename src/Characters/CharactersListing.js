import React from 'react'
import { connect } from 'react-redux'

import Listing from "../common/Listing"
import { dispatch } from '../common/store/'
import { PageTitle } from '../common/styled'

import { actions } from './charactersActions'

function CharactersListing (props) {
  React.useEffect(() => {
    props.fetchAllCharacters()
  }, [props.fetchAllCharacters])

  const columns = [
    {title: 'Name', value: 'name'},
    {title: 'Gender', value: 'gender'},
    {title: 'Height', value: 'height'},
    {title: 'Mass', value: 'mass'},
  ]

  return (
    <>
      <PageTitle>Characters</PageTitle>

      <Listing
        columns={columns}
        data={props.data}
        error={props.error}
        failed={props.isFailed}
        loading={props.isLoading}
      />
    </>
  )
}

function mapStateToProps({ Characters }) {
  return {
    data: Characters?.list?.results,
    error: Characters?.list?.meta?.error,
    isFailed: Characters?.list?.meta?.isFailed,
    isLoading: Characters?.list?.meta?.isLoading,
    isCompleted: Characters?.list?.meta?.isCompleted,
  }
}

function mapDispatchToProps(ownProps) {
  return {
    fetchAllCharacters: (...args) => {
      console.log('[CharactersListing]fetchAllCharacters', args)
      dispatch(actions.fetchAllCharacters(...args))

      // eslint-disable-next-line no-unused-expressions
      ownProps?.fetchAllCharacters?.(...args)
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharactersListing)