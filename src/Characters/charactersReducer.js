import { inCase } from '../common/utils'
import { actionTypes } from './charactersActions'

const defaultState = {
	list: {
		results: [],
		meta: {},
	},
	meta: {},
}

export default inCase.curry('charactersReducer')
	.condition((_, action) => action.type)
	.otherwise((state) => state || defaultState)
	.cases({
		[actionTypes.FETCH_ALL_CHARACTERS]: fetchAllCharacters,
		[actionTypes.FETCH_ALL_CHARACTERS_FAILED]: fetchAllCharactersFailed,
		[actionTypes.FETCH_ALL_CHARACTERS_COMPLETED]: fetchAllCharactersCompleted,
	})

//
function fetchAllCharacters(store, action) {
	console.log('[charactersReducer] fetchAllCharacters::action', action)

	return {
		...store,
		list: {
			...store.list,
			meta: {
				...store.list?.meta,
				isLoading: true,
				isRequested: true,

				isFailed: null,
				isCompleted: null,
			},
		},
	}
}

function fetchAllCharactersCompleted(store, action) {
	console.log('[charactersReducer] fetchAllCharactersCompleted::action', action)

	return {
		...store,
		list: {
			...store.list,
			results: action?.payload,
			meta: {
				...store.list?.meta,
				isLoading: false,
				isRequested: null,

				isFailed: false,
				isCompleted: true,
			},
		},
	}
}

function fetchAllCharactersFailed(store, action) {
	console.log('[developersReducer] fetchAllServicesFailed::action', action)

	return {
		...store,
		list: {
			...store.list,
			meta: {
				...store.list?.meta,
				isLoading: false,
				isRequested: null,

				isFailed: true,
				isCompleted: false,

				error: action.payload,
			},
		},
	}
}
