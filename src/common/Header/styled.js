import styled from 'styled-components'

export const MainTitle = styled.h1`
    float:left;
    text-align:left;
    
    font-size: 25px;
    margin: 13px 6px;
    
    color: #4c4c4c;
`

export const MainHeader = styled.div`
    float: left;
    width: 100%;
    height: 60px;
    text-align: center;
    margin-bottom: 15px;
    background-color: #000;
    img{
        float: left;
        height: 40px;
        margin-top: 10px;
        margin-left: 15px;
    }
`
