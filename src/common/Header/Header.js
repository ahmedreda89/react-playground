import React from "react"
import { Link } from "react-router-dom"
import noInternet from 'no-internet'

import logo from '../assets/starsLogo.svg.png'
import logoDimmed from '../assets/starsLogoDimmed.svg.png'
import { MainHeader } from './styled'


export default function Header (props) {
  const [isOnline, setIsOnline] = React.useState(true)

  React.useEffect(() => {
    function noInternetCallbackHandling(offline) {
      return offline
        ? setIsOnline(false)
        : setIsOnline(true)
    }

    // connection is checked and callback is called each 5000 milliseconds
    noInternet({ callback: noInternetCallbackHandling })
  },[])

  const renderLogo = () => {
    return isOnline
      ? (
        <Link to="/home">
          <img src={logo} className="App-logo" alt="logo" />
        </Link>
      )
      : (
        <Link to="/home">
          <img src={logoDimmed} className="App-logo" alt="logo" />
        </Link>
      )
  }

  return (
    <MainHeader>
      {renderLogo()}
      {/*<MainTitle>{'Wohoooooooooooooooooooo'}</MainTitle>*/}
    </MainHeader>
  )
}