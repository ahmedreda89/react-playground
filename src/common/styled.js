import styled from 'styled-components'

export const PageTitle = styled.h2`
    color: #000;
    font-size: 21px;
`

export const PageWrapper = styled.div`
    width: 80%;
    font-size: 13px;
    margin-left: 10%;
`

export const ErrorBlock = styled.div`
	padding: 10px;
  color: #a00c0c;
  background: #ffe2e2;
`
export const SuccessBlock = styled.div`
	padding: 10px;
  color: #105800;
  background: #e0ffe4;
`
