import inCase from './switchCase'
import { renameFunc, observableFromPromiseProperties } from './utils'

export {
	inCase,
	renameFunc,
	observableFromPromiseProperties,
}