export function renameFunc(func, name) {
  /* eslint-disable no-param-reassign */
  Object.defineProperty(func, 'name', { writable: true })
  func.name = name
  Object.defineProperty(func, 'name', { writable: false })
  /* eslint-enable no-param-reassign */

  return func
}

export function observableFromPromiseProperties(obj, rx) {
  return Object
    .entries(obj)
    .reduce((a, [ky, val]) => {
      a[ky] = (...args) => rx.from(val(...args))
      return a
    }, {})
}