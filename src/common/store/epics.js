import { combineEpics } from 'redux-observable'

import planetsEpic from '../../Planets/planetsEpic'
import charactersEpic from '../../Characters/charactersEpic'


export default combineEpics(...[
    ...planetsEpic,
    ...charactersEpic,
])