import configureStore from './configureStore'

const store = configureStore()

export const {
    dispatch,
    getState,
} = store

export default store