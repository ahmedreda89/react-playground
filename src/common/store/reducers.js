import { combineReducers } from 'redux'

import planetsReducer from '../../Planets/planetsReducer'
import charactersReducer from '../../Characters/charactersReducer'


const rootReducer = combineReducers({
    // App: appReducer,
    // Home: homeReducer,
    Planets: planetsReducer,
    Characters: charactersReducer,
    // Spaceships: spaceshipsReducer,
})

export default rootReducer
