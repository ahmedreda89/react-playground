import styled from 'styled-components'

export const Grid = styled.div``

export const GridHeader = styled.div`
    background: #ddd;
`

export const GridRow = styled.div`
    border-bottom: 1px solid #ddd;
`
