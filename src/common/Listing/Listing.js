import React from "react"

import { ErrorBlock } from '../styled'
import { Grid, GridHeader, GridRow } from './styled'

export default function Listing (props) {
  const autoColumnsWidth = (100 / props.columns?.length) + '%'
  const cellStyles = {
    padding: '5px 10px',
    width: autoColumnsWidth,
    display: 'inline-block',
    boxSizing: 'border-box',
  }

  const renderColumnsCells = () => {
    return props.columns?.map((item) => {
      return (
        <div style={cellStyles}>
          {item.title}
        </div>
      )
    })
  }

  const renderListingHeader = () => {
    return (
      <GridHeader>
        {renderColumnsCells()}
      </GridHeader>
    )
  }

  const renderListingRows = () => {
    return props.data?.map((rowData) => {
      const selectedData = props.columns.map((column) => {
        return (rowData[column?.value])
          ? <span style={cellStyles}>{rowData[column?.value]}</span>
          : null
      })

      return <GridRow>{selectedData}</GridRow>
    })
  }

  const renderLoading = () => {
    return props.loading ? 'Loading..' : null
  }

  const renderGrid = () => {
    if(!props.failed) {
      return (
        <>
          {renderListingHeader()}
          {renderListingRows()}
          {renderLoading()}
        </>
      )
    }
  }

  const renderError = () => {
    console.log('[Listing] renderError: error', props.error)

    const genericError = 'Something went wrong, please try again later.'

    return props.failed
      ? <ErrorBlock>{props.error || genericError}</ErrorBlock>
      : null
  }

  return (
    <Grid>
      {renderGrid()}
      {renderError()}
    </Grid>
  )
}